package com.example.ges;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.google.android.material.snackbar.Snackbar;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Login extends AppCompatActivity {

    private ProgressDialog pDialog;
    JSONParser jsonParser = new JSONParser();
    private static String url_create_user = "https://ges-back.herokuapp.com/api/login";
    private static final String TAG_SUCCESS = "success";
    Snackbar snackbar;
    RelativeLayout snackView;
    Button ini_ses, crearCuenta;
    public EditText usuario, contra;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        snackView = (RelativeLayout) findViewById(R.id.activity_login);
        ini_ses =findViewById(R.id.iniciar_sesion);
        crearCuenta =findViewById(R.id.crear_cuenta);
        usuario = (EditText) findViewById(R.id.correo);
        contra = (EditText) findViewById(R.id.password);

        ini_ses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                new IniciarSesion().execute();
            }
        });

        crearCuenta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                startActivity(new Intent(Login.this, Registrar.class));
                finish();
            }
        });

    }

    class IniciarSesion extends AsyncTask<String, String, String> {
        /**
         * Before starting background thread Show Progress Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Login.this);
            pDialog.setMessage("Iniciando Sesión...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        /**
         * Creating product
         */
        protected String doInBackground(String... args) {
            String user = usuario.getText().toString();
            String pas = contra.getText().toString();
            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("Correo", user));
            params.add(new BasicNameValuePair("password", pas));
            // getting JSON Object
            // Note that create product url accepts POST method
            try {


                JSONObject json = jsonParser.makeHttpRequest(url_create_user,
                        "POST", params);
                // check log cat fro response
                Log.d("Create Response", json.toString());
                // check for success tag


                if(json.get("status").equals("invalid_credentials")){
                    snackbar = Snackbar.make(snackView, "Tu usuario o clave es incorrecto!!",Snackbar.LENGTH_LONG);
                    snackbar.show();
                    return null;
                }
                if (json.has("status")) {
                    String status = json.getString("status");
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            startActivity(new Intent(Login.this, Menu.class));
            finish();
            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog once done
            pDialog.dismiss();

        }


    }
}