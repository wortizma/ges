package com.example.ges;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.google.android.material.snackbar.Snackbar;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


public class Registrar extends AppCompatActivity {

    private ProgressDialog pDialog;
    Snackbar snackbar;
    JSONParser jsonParser = new JSONParser();
    private static String url_create_user = "https://ges-back.herokuapp.com/api/register";
    private static final String TAG_SUCCESS = "success";

    Button registrar, regresar_sesion;
    RelativeLayout snackView;
    public EditText Nombre, Apellidos, Telefono, Correo, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar);
        snackView = (RelativeLayout) findViewById(R.id.activity_login);
        Nombre = (EditText) findViewById(R.id.nombre);
        Apellidos = (EditText) findViewById(R.id.apellido);
        Telefono = (EditText) findViewById(R.id.telefono);
        Correo = (EditText) findViewById(R.id.correo);
        password = (EditText) findViewById(R.id.password);
        registrar = findViewById(R.id.registrar);
        regresar_sesion = findViewById(R.id.regresar_sesion);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        registrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new CreateNewUser().execute();

            }
        });

        regresar_sesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Registrar.this, Login.class));
                finish();
            }
        });
    }

    class CreateNewUser extends AsyncTask<String, String, String> {
        /**
         * Before starting background thread Show Progress Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Registrar.this);
            pDialog.setMessage("Guardando el registro...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        protected String doInBackground(String... args) {
            String NombreA = Nombre.getText().toString();
            String ApellidosA = Apellidos.getText().toString();
            String TelefonoA = Telefono.getText().toString();
            String CorreoA = Correo.getText().toString();
            String passwordA = password.getText().toString();
            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("Nombre", NombreA));
            params.add(new BasicNameValuePair("Apellidos", ApellidosA));
            params.add(new BasicNameValuePair("Telefono", TelefonoA));
            params.add(new BasicNameValuePair("Correo", CorreoA));
            params.add(new BasicNameValuePair("password", passwordA));
            JSONObject json = jsonParser.makeHttpRequest(url_create_user,
                    "POST", params);

                if(json.isNull("status")){
                    snackbar = Snackbar.make(snackView, "El usuario no fue creado",Snackbar.LENGTH_LONG);
                    snackbar.show();
                    return null;
                }

            startActivity(new Intent(Registrar.this, Login.class));
            finish();
            return null;
        }
        
        protected void onPostExecute(String file_url) {
            // dismiss the dialog once done
            pDialog.dismiss();

        }


    }
}